sigma_faust
===========

Code to reproduce experiments in preprint *Learning a sum of sparse matrix
products* by Moujahid Bou-Laouz, Valentin Emiya, Liva Ralaivola and Caroline
Chaux (currently under review).

Install
-------

Install requirements by creating an ``anaconda`` environment with Python 3.7
or 3.9:

    conda create -n sigma_faust python=3.7 pandas numpy matplotlib scipy scikit-learn sympy openpyxl

or

    conda create -n sigma_faust python=3.9 pandas numpy matplotlib scipy scikit-learn sympy openpyxl

Then activate the environment and install ``pyfaust``:

    conda activate sigma_faust

    pip install pyfaust

Download and extract 'sigma_faust' files, go to the source code root directory
and run

    pip install -e .


Running the code
----------------

To reproduce the first experiment and the top two plots of Figure 1 (target
matrix is composed of random entries), execute file `exp_icassp_random.py` to
run the experiment and execute function `plot_random(N=256)` from
`sigma_faust.plot_icassp_results` module to display the results.

To reproduce the second experiment, the bottom plot of Figure 1 and Figure 2
(target matrix is composed of data from MNIST),

* download and locally extract the data from the `UCI repository <https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits>`_
* run file `extract_matrices_from_datasets.py` after adapting path variable `root_dir` depending on the location of your data
* run file `exp_icassp_data_matrices.py`.
* execute function `plot_digits()` from `sigma_faust.plot_icassp_results` module to display the results.

Bug report
----------

This code is in a preliminary version. Please report any bug to the authors.