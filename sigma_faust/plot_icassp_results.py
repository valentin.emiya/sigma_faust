# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""

.. moduleauthor:: Valentin Emiya
"""
import pickle
import numpy as np
import matplotlib.pyplot as plt


def plot_results(relative_error, n_terms_list, filename, title,
                 additional_curves=False, curve_style='o:'):
    plt.figure()
    k = 'PALM4SMSA Id'
    plt.plot(n_terms_list, relative_error[k], curve_style,
             label='PALM4$\Sigma$MSA')
    if additional_curves:
        k = 'PALM4MSA Id'
        plt.plot(1, relative_error[k], '+', label=k)

    if additional_curves:
        k = 'PALM4SMSA Rd'
        # plt.errorbar(n_terms_list,
        #              [np.mean(x) for x in relative_error[k]],
        #              [np.std(x) for x in relative_error[k]],
        #              label=k)
        plt.plot(n_terms_list, [np.mean(x) for x in relative_error[k]], label=k)
        k = 'PALM4MSA Rd'
        # plt.errorbar(1, np.mean(relative_error[k]), np.std(relative_error[k]),
        #              marker='x', label=k)
        plt.plot(1, np.mean(relative_error[k]), marker='x', label=k)

    k = 'G-PALM4SMSA Id'
    plt.plot(n_terms_list, relative_error[k], curve_style,
             label='Greedy PALM4$\Sigma$MSA')

    if additional_curves:
        k = 'G-PALM4SMSA Rd'
        # plt.errorbar(n_terms_list,
        #              [np.mean(x) for x in relative_error[k]],
        #              [np.std(x) for x in relative_error[k]],
        #              label=k)
        plt.plot(n_terms_list, [np.mean(x) for x in relative_error[k]], label=k)
    k = 'G-PALM4SMSA H'
    print(title, np.min(relative_error[k]))
    plt.plot(n_terms_list, relative_error[k], curve_style,
             label='Greedy PALM4$\Sigma$MSA H')
    if additional_curves:
        k = 'H-PALM4MSA'
        plt.plot(1, relative_error[k], '.', label=k)
    ry = 0.05
    plt.legend()
    plt.grid()
    plt.xlabel('Number of terms ($K$)')
    plt.ylabel('Relative MSE')
    yl = plt.ylim()
    plt.ylim(0-ry, min(1+ry, yl[1]))
    plt.title(title)
    # plt.yscale('log')
    if additional_curves:
        plt.savefig(f'{filename}_extended.pdf')
    else:
        plt.savefig(f'{filename}.pdf')
    plt.close()


def plot_random(N=None):
    if N == 256:
        with open('2021-10-05_res_icassp_nb_factors.pickle', 'rb') as file:
            res = pickle.load(file)
    else:
        with open('res_icassp_nb_factors.pickle', 'rb') as file:
            res = pickle.load(file)

    for data_size, n_factors in res:
        if N is not None and N != data_size:
            continue
        relative_error, n_terms_list = res[data_size, n_factors]
        filename = f'icassp_{data_size}_{n_factors}'
        title = f'Gaussian matrix $N={data_size}$ $B={n_factors}$'
        for additional_curves in (False, True):
            plot_results(relative_error, n_terms_list,
                         filename=filename, title=title, curve_style='o:',
                         additional_curves=additional_curves)
            plot_results(relative_error, n_terms_list,
                         filename=filename + '_o', title=title, curve_style='o',
                         additional_curves=additional_curves)


def plot_random256():
    with open('res_icassp_nb_factors_datasets_copie.pickle', 'rb') as file:
        res = pickle.load(file)

    for data_size, n_factors in res:
        relative_error, n_terms_list = res[data_size, n_factors]
        filename = f'icassp_{data_size}_{n_factors}'
        title = f'Gaussian matrix $N={data_size}$ $B={n_factors}$'
        for additional_curves in (False, True):
            plot_results(relative_error, n_terms_list,
                         filename=filename, title=title, curve_style='o:',
                         additional_curves=additional_curves)
            plot_results(relative_error, n_terms_list,
                         filename=filename + '_o', title=title, curve_style='o',
                         additional_curves=additional_curves)


def plot_digits():
    with open('res_icassp_nb_factors_datasets.pickle', 'rb') as file:
        res = pickle.load(file)

    for k in res:
        dataset, data_size, n_factors = k
        if dataset != 'digits' or n_factors != 6:
            continue
        relative_error, n_terms_list, M_est = res[k]
        filename = f'icassp_{dataset}_{data_size}_{n_factors}'
        title = f'Dataset MNIST $N={data_size}$ $B={n_factors}$'
        additional_curves = False
        plot_results(relative_error, n_terms_list,
                     filename=filename, title=title,
                     additional_curves=additional_curves,
                     curve_style='o:')
        plot_results(relative_error, n_terms_list,
                     filename=filename+'_o', title=title,
                     additional_curves=additional_curves,
                     curve_style='o')

        n_examples = 10
        for i, M in enumerate(M_est):
            for n in range(n_examples):
                plt.figure()
                plt.imshow(-np.reshape(M[n, :], (8, 8)), cmap='gray')
                frame = plt.gca()
                frame.axes.get_xaxis().set_visible(False)
                frame.axes.get_yaxis().set_visible(False)
                plt.savefig(f'digits{n}_K{n_terms_list[i]}.pdf',
                            bbox_inches='tight')
                plt.close()
        M = np.load(f'data/digits.npy')
        for n in range(n_examples):
            plt.figure()
            plt.imshow(-np.reshape(M[n, :], (8, 8)), cmap='gray')
            frame = plt.gca()
            frame.axes.get_xaxis().set_visible(False)
            frame.axes.get_yaxis().set_visible(False)
            plt.savefig(f'digits{n}_ref.pdf', bbox_inches='tight')
            plt.close()
        w = '.1\\textwidth'
        for n in range(n_examples):
            print(f'\includegraphics[width={w}]'
                  + '{' + f'figures/digits{n}_ref.pdf' + '}')
            print(f'\includegraphics[width={w}]'
                  + '{' + f'figures/digits{n}_K1.pdf' + '}')
            print(f'\includegraphics[width={w}]'
                  + '{' + f'figures/digits{n}_K3.pdf' + '}')
            print(f'\includegraphics[width={w}]'
                  + '{' + f'figures/digits{n}_K6.pdf' + '}')
            print('\\\\')


if __name__ == '__main__':
    plot_digits()
    plot_random()
    plot_random(128)
    plot_random(256)