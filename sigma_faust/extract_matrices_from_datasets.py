# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""

.. moduleauthor:: Valentin Emiya
"""
from pathlib import Path

import numpy as np
import pandas as pd
import sklearn.datasets as skldata


def extract_matrix(dataset, root_dir, out_dir=Path('./data')):
    Path(out_dir).mkdir(exist_ok=True)
    if dataset == 'LSVT_voice_rehabilitation':
        root_dir = root_dir / 'LSVT_voice_rehabilitation'
        df = pd.read_excel(root_dir / 'LSVT_voice_rehabilitation.xlsx')
        M = df.to_numpy()[:126, :126]
    elif dataset == 'digits':
        data = skldata.load_digits()
        M = data['data']
    else:
        raise ValueError(f'Unknown dataset {dataset}')

    data_dim = np.min(M.shape)
    M = M[:data_dim, :data_dim]
    np.save(out_dir / f'{dataset}.npy', M)


if __name__ == '__main__':
    # Adapt this path depending on the location of your data
    root_dir = Path('/Users/valentin/data/UCI/icassp21')
    for dataset in ('LSVT_voice_rehabilitation', 'digits'):
        try:
            extract_matrix(dataset=dataset, root_dir=root_dir)
        except ValueError as e:
            print(e)
        except FileNotFoundError as e:
            print(f'Dataset {dataset} not available: {e}')
