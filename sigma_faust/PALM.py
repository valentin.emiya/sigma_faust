# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""
Created on Tue May  16 13:23:40 2021

 @author: Moujahid 
"""


from pyfaust import Faust
from pyfaust.fact import palm4msa
from pyfaust.factparams \
    import ParamsPalm4MSA, ConstraintList, StoppingCriterion
import pyfaust
import numpy as np
from numpy import linalg as LA
from scipy.sparse import random
from numpy.linalg import multi_dot
from scipy.stats import special_ortho_group
from random import randint 
import math



def asfortranarray(A):
    
    return [np.asfortranarray(i) for i in A]

def asfortranarray2(A):
    return [asfortranarray(A[i]) for i in range(len(A))]


def f(A):
    #lors de la réinitilasition sur chaques itérations, des fois les facteurs sont denses/creuses
    if type(A) is np.ndarray:
        return A
    else:
        return A.toarray()


def Faust_to_list(F):
    return [np.asfortranarray(f(F.factors(i))) for i in range(F.numfactors())]

def Erreur_r(A,B):
    return LA.norm(A-B)/LA.norm(A)

def multiplication(A,_lambda=1):
    if len(A)==1:
        return _lambda*A[0]
    else:
        return _lambda*multi_dot(A)   


def somme_multiplication(A,_lambda):
    "A est une liste de liste"
    "_lambda est une liste"
    if len(A)==0:
        return 0
    if len(_lambda)==0:
        _lambda=[1]*len(A)
    S=[]
    K=len(A)
    for s in range(0,K):
        S.append(multiplication(A[s],_lambda[s]))
    S=sum(S)
    return S



#Palm pour le nouveau modèle 
#PALM2
def Palm4smsa(A,K,J,cons,initfacts,initlambda,N,threshold=1e-3):
    """
    Parameters

    ----------
     A : Matrice a décomposer 
    K : somme
    J : produit
    cons : une liste dont chaque élément est un objet de la classe ConstraintList
    initfacts : Initilisation de tout les KJ facteurs , une liste de K liste de J éléments
    initlambda : Initialisation des scalaires lambda , une liste de K éléments 
    N : Nombre d'itérations 

    Returns
    -------
    F : facteurs
    _lambda : scalaires
    historique_lambda : valeurs des lambdas sur chaque itération
    historique_facteurs : valeurs des facteurs sur chaque itération
    erreur_relatif: erreur_relatif sur chaque itération
    I : nombre d'itérations néccesaire pour atteindre le seuil  
    
    
    """
    
    
    stop_crit=StoppingCriterion(num_its=1)
    
    historique_lambda=[initlambda]
    historique_facteurs=[initfacts]
    S=somme_multiplication(initfacts,initlambda)
    erreur_relatif=[LA.norm(A-S)/LA.norm(A)]
    for i in range(N):
        initlambda1=list(historique_lambda[len(historique_lambda)-1])
        initfacts1=list(historique_facteurs[len(historique_facteurs)-1])
        for k in range(K):
            R=somme_multiplication(initfacts1[:k],initlambda1[:k])+somme_multiplication(initfacts1[k+1:],initlambda1[k+1:])
            param=ParamsPalm4MSA(cons[k],stop_crit,init_facts=initfacts1[k],\
                                 init_lambda=initlambda1[k],is_update_way_R2L=True)
            M=A-R
            init_factsu,lambdau=palm4msa(M,param,ret_lambda=True)
            X=Faust_to_list(init_factsu)
            # On doit normaliser les facteurs de init_factsu, on divise le facteur qui a une norme différente de 1 par lambdau
            X=[X[i] if np.isclose(LA.norm(X[i]),1,atol=1e-03) else X[i]/lambdau for i in range(len(X))]
            initfacts1[k]=asfortranarray(X)
            initlambda1[k]=lambdau

        
        historique_lambda.append(initlambda1)
        historique_facteurs.append(initfacts1)
        S=somme_multiplication(initfacts1,initlambda1)
        erreur_relatif.append(LA.norm(A-S)/LA.norm(M))
        if abs(erreur_relatif[i]-erreur_relatif[i-1])<threshold:
                break
    print("l'algorithme a convergé pendant %d iteration" %(i+1))
    I=i+1
    F=historique_facteurs[i]
    _lambda=historique_lambda[i]
    return F,_lambda,erreur_relatif,historique_facteurs,historique_lambda,I



# On doit normaliser le facteur de init_factsu, on divise le faust qui a norme différente de 1 par lambdau
     

#Exemple : test : initlisation dans un minimum local 
if __name__ == "__main__":
    import matplotlib.pyplot as plt
    d=3
    S_1=np.array([[1,2,0],[0,0,4],[0,0,0]],dtype=float)
    S_2=np.array([[1,0,0],[0,2,0],[0,0,4]],dtype=float)
    M1=np.dot(S_1,S_2)
    S_3=np.array([[0,0,3],[1,1,2],[0,0,0]],dtype=float)
    S_4=np.array([[0,1,1],[4,0,2],[4,0,0]],dtype=float)
    M2=np.dot(S_3,S_4)
    print(M1+M2)
    M=M1+M2
    s_1=np.count_nonzero(S_1)
    s_2=np.count_nonzero(S_2)
    s_3=np.count_nonzero(S_3)
    s_4=np.count_nonzero(S_4)
    #Normalisation :
    B_1=S_1/LA.norm(S_1)
    B_2=S_2/LA.norm(S_2)
    B_3=S_3/LA.norm(S_3)
    B_4=S_4/LA.norm(S_4)
    
    #Initilalisation de lambda
    N=100
    K=1
    J=2
    C=[ConstraintList('sp',s_1,d,d,'sp',s_2,d,d),\
      ConstraintList('sp',s_3,d,d,'sp',s_4,d,d)]
              
    B=[[B_1,B_2],[B_3,B_4]]
    L=[LA.norm(S_1)*LA.norm(S_2),LA.norm(S_3)*LA.norm(S_4)]
    A=Palm4smsa(M,K=2,J=2,cons=C,initfacts=asfortranarray2(B),initlambda=L,N=N)
    
    
    
   
    
    # 2eme exemple : test de convergence 
    d=3
    D=np.diag((4.1,4,5))
    S_1
    M=D+S_1
    s_1=np.count_nonzero(S_1)
    d_1=np.count_nonzero(D)
    M=S_1+D
    
    B_1=S_1/LA.norm(S_1)
    D_1=D/LA.norm(D)
    N=100
    K=2
    C=[ConstraintList('sp',s_1,d,d,'sp',s_1,d,d),\
       ConstraintList('sp',d_1,d,d)]
              
    B=[[B_1,B_1],[D_1]]
    L=[LA.norm(S_1),LA.norm(D)]
    P1=Palm4smsa(M,K=2,J=1,cons=C,initfacts=asfortranarray2(B),initlambda=L,N=N)
    
    
    
    
    n=range(P1[5]+1)
            
    fig = plt.figure(2)
    plt.plot(n,P1[2])
    plt.title('test de convergence')
    
    
    #Exemple aléatoire 
    d=100
        
    S_1=random(d,d,density=0.1)
    S_2=random(d,d,density=0.1)
    Y=3*S_1.dot(S_2).toarray()
    K=3
    J=2
    N=30
    C=[ConstraintList('skperm',2,d,d,'skperm',2,d,d),\
          ConstraintList('skperm',2,d,d,'skperm',2,d,d),\
               ConstraintList('skperm',2,d,d,'skperm',2,d,d)]
              
    
             
    B=[[1/2*np.identity(d),np.zeros((d,d))]]*3
    A=[1.0,1.0,1.0]
    P2=Palm4smsa(Y,K,J,cons=C,initfacts=asfortranarray2(B),initlambda=A,N=500,threshold=1e-4)

    #Plot : itérations vs erreur
    n=range(P2[5]+1)
        
    fig = plt.figure(3)
    plt.plot(n,P2[2])
    plt.title("Fonction objectif")
    plt.xlabel("Itération")
    plt.ylabel("Erreur relative")
    
    
    


    







