# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""
Created on Wed May 26 12:13:20 2021

@author: Admin
"""
import copy

from pyfaust.fact import hierarchical
from pyfaust.proj import *

from sigma_faust.PALM import * # fichier


def Palm4smsa_hierarchique(A,K,J,cons,initfacts,initlambda,N,N_1=None):
    '''
    Décomposition using palm4msa
    Parameters
    ----------
    A : array a décomposer
    K : nombre de terme 
    J : nombre de facteur 
    cons : contrainte, une liste de liste objet : pyfaust.proj
    initfacts : liste de liste, initialisation facteur
    initlambda : liste initisialisation scalaire 
    N : Nombre d'itération palm4msa
    N_1 : Nombre d'itération palm4smsa

    Returns
    -------
    Factors
    scalars 

    '''
    stop_crit=StoppingCriterion(num_its=N)
    Factors=[]
    scalars=[]
    for k in range(K):
        if np.allclose(A-somme_multiplication(Factors,scalars),np.zeros(A.shape))==True:
           break
        else:
            param=ParamsPalm4MSA(cons[k],stop_crit,init_facts=initfacts[k],\
                                     init_lambda=initlambda[k],is_update_way_R2L=True)
            F,_lambda=palm4msa(A-somme_multiplication(Factors,scalars),param,ret_lambda=True)
            F1=Faust_to_list(F)
            F1=[F1[i] if np.isclose(LA.norm(F1[i]),1,atol=1e-03) else F1[i]/_lambda for i in range(len(F1))]
            Factors.append(F1)
            scalars.append(_lambda)
        if N_1!=None:
            if k==0:
                continue
            else:
                F2,_lambda2,U=Palm4smsa(A,k+1,J,cons[:k+1],Factors,scalars,N_1)[:3]
                Factors=F2
                scalars=_lambda2
            
    return Factors,scalars


def Palm4smsa_hierarchique2(A,K,J,fact_cons,res_cons,N_1,N_2,N_3=None):
    '''
    Décomposition using hierarchical palm4msa : initilisation not possible : pas très grave puisque le palm4msa n'est pas sensible a l'iniisailisation 
    Parameters
    ----------
    A : array a décomposer
    K : nombre de terme 
    J : nombre de facteur 
    fact_cons : contrainte des facteurs, une liste de liste objet : pyfaust.proj
    res_cons : contrainte des résidus, une liste de liste objet : pyfaust.proj
    initlambda : liste initisialisation scalaire 
    N_1 : Nombre d'itération palm4msa
    N_2 : Nombre d'itération palm4msa global
    N_3 : Nombre d'itération palm4smsa

    Returns
    -------
    Factors
    scalars 

    '''
    stop_crit1=StoppingCriterion(num_its=N_1)
    stop_crit2=StoppingCriterion(num_its=N_2)
    cons=copy.deepcopy(fact_cons)
    # si on utilisie *k on a :
    cons[0].append(res_cons[0][-1])
    Factors=[]
    scalars=[]
    for k in range(K):
        if np.allclose(A-somme_multiplication(Factors,scalars),np.zeros(A.shape))==True:
           break
        else:
            param=ParamsHierarchical(fact_cons[k], res_cons[k], stop_crit1, stop_crit2,is_update_way_R2L=True)
            F,_lambda=hierarchical(A-somme_multiplication(Factors,scalars),param,ret_lambda=True)
            F1=Faust_to_list(F)
            F1=[F1[i] if np.isclose(LA.norm(F1[i]),1,atol=1e-03) else F1[i]/_lambda for i in range(len(F1))]
            Factors.append(F1)
            scalars.append(_lambda)
        if N_3!=None:
            if k==0:
                continue
            else:
                F2,_lambda2=Palm4smsa(A,k+1,J,cons[:k+1],Factors,scalars,N_3)[:2]
                Factors=F2
                scalars=_lambda2
                
    return Factors,scalars

if __name__ == "__main__":
    #test
    d=10
    
    M=10*random(d,d,density=1).toarray()
    K=2
    J=6
    D=[[splincol(M.shape,2) for i in range(6)],[splincol(M.shape,2) for i in range(6)]]
    
    
    I=[[np.identity(d)]*6,[M]*6]
    A=[1.0,1.0]
    H1,H2=Palm4smsa_hierarchique(M,K,J,cons=D,initfacts=asfortranarray2(I),initlambda=A,N=500,N_1=5000)
    print(LA.norm(M-somme_multiplication(H1,H2))/LA.norm(M))
    K=2
    J=3
    F=[[splincol(M.shape, 2) for i in range(2)]]*2
    R=[[splincol(M.shape,10),splincol(M.shape,2)]]*2
    H3,H4=Palm4smsa_hierarchique2(M,K,J,fact_cons=F,res_cons=R,N_1=500,N_2=500,N_3=500)
    print(LA.norm(M-somme_multiplication(H3,H4))/LA.norm(M))
