# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""

.. moduleauthor:: Valentin Emiya
"""
import numpy as np

from sigma_faust.exp_icassp_nb_factors import experience, get_list_of_n_factors
from sigma_faust.PALM import somme_multiplication


if __name__ == '__main__':
    print(get_list_of_n_factors(32))
    print(get_list_of_n_factors(64))
    print(get_list_of_n_factors(128))
    print(get_list_of_n_factors(100))

    import pickle

    res = {}
    for dataset in ['digits', 'LSVT_voice_rehabilitation', 'breast_cancer']:
        M = np.load(f'data/{dataset}.npy')
        assert M.shape[0] == M.shape[1]
        data_size = M.shape[0]
        print('*' * 80)
        print(f'Dataset {dataset} - Data size {data_size}')
        print('*' * 80)
        for n_factors in get_list_of_n_factors(data_size)[:2]:
            print('=' * 60)
            print(f'{n_factors} factors')
            print('=' * 60)
            if n_factors * data_size * 2 > data_size ** 2:
                break
            relative_error, n_terms_list, res_factors = \
                experience(data_dim=data_size, n_factors=n_factors,
                           n_runs_init=0, data_type=dataset)
            M_est = [somme_multiplication(*f) for f in res_factors]
            res[dataset, data_size, n_factors] = \
                relative_error, n_terms_list, M_est
            # filename = f'icassp_{dataset}_{data_size}_{n_factors}'
            # title = f'Data size : {data_size} - Budget : {n_factors}'
            # for additional_curves in (False, True):
            #     plot_results(relative_error, n_terms_list,
            #                  filename=filename, title=title,
            #                  additional_curves=additional_curves)
            with open('res_icassp_nb_factors_datasets.pickle', 'wb') as file:
                pickle.dump(res, file)
