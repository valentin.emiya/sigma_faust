# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""

.. moduleauthor:: Valentin Emiya
"""
import numpy as np
import numpy.linalg as LA
from sympy import divisors

from pyfaust.proj import splincol

from sigma_faust.hierarchique \
    import StoppingCriterion, ParamsPalm4MSA, ParamsHierarchical, \
    asfortranarray, asfortranarray2, \
    palm4msa, hierarchical, \
    Palm4smsa, Palm4smsa_hierarchique, Palm4smsa_hierarchique2, \
    somme_multiplication


def compute_relative_error(est_matrix, ref_matrix):
    return LA.norm(ref_matrix - est_matrix) / LA.norm(ref_matrix)


def experience(data_dim=32, n_factors=5, n_runs_data=1, n_runs_init=10,
               data_type='randn'):
    """

    Parameters
    ----------
    data_dim : taille de la matrice aléatoire
    n_factors : Nombre de facteurs

    Returns
    -------
    la fonction génére un graphe de comparaison des algos

    """
    if n_runs_data > 1:
        raise NotImplementedError()

    lincol_sparsity = 2  # number of non-zero per row and col
    n_iter_max = 30
    n_iter_max = 100
    n_terms_list = divisors(n_factors)
    # n_terms_list.remove(n_factors)
    # n_terms_list = [n_factors,]
    relative_error = {}

    # Data generation
    if data_type == 'randn':
        M = np.random.randn(data_dim, data_dim)
    else:
        M = np.load(f'data/{data_type}.npy')
        assert M.shape[0] == M.shape[1]
        data_dim = M.shape[0]

    # run PALM4MSA with Id/0 initialisation
    cons = [splincol((data_dim, data_dim), lincol_sparsity)
            for _ in range(n_factors)]
    stop_crit = StoppingCriterion(num_its=n_iter_max)
    param = ParamsPalm4MSA(cons, stop_crit, constant_step_size=False)
    # param = ParamsPalm4MSA(cons, stop_crit, constant_step_size=True)
    # Factorisation avec palm4msa avec l'inisialisation naturelle
    M_est = palm4msa(M, param).toarray()
    relative_error['PALM4MSA Id'] = compute_relative_error(est_matrix=M_est,
                                                           ref_matrix=M)

    # run PALM4MSA with random initialisation
    relative_error['PALM4MSA Rd'] = []
    # Random comme intiliastion
    for i in range(n_runs_init):
        I_Factors = [np.random.randn(data_dim, data_dim)] * n_factors
        param = ParamsPalm4MSA(cons, stop_crit,
                               init_facts=asfortranarray(I_Factors),
                               is_update_way_R2L=True,
                               constant_step_size=True)
        param = ParamsPalm4MSA(cons, stop_crit,
                               init_facts=asfortranarray(I_Factors),
                               is_update_way_R2L=True,
                               constant_step_size=False)
        M_est = palm4msa(M, param).toarray()
        relative_error['PALM4MSA Rd'].append(
            compute_relative_error(est_matrix=M_est, ref_matrix=M))

    # run Hierarchical PALM4MSA
    fact_cons = [splincol(M.shape, lincol_sparsity)
                 for _ in range(n_factors - 1)]
    res_cons = [splincol(M.shape, int(data_dim / (2 + i)))
                for i in range(n_factors - 2)] \
               + [splincol(M.shape, lincol_sparsity)]
    stop_crit1 = StoppingCriterion(num_its=n_iter_max)
    stop_crit2 = StoppingCriterion(num_its=n_iter_max)
    param = ParamsHierarchical(fact_cons, res_cons,
                               stop_crit1, stop_crit2,
                               is_update_way_R2L=True)
    M_est = hierarchical(M, param).toarray()
    relative_error['H-PALM4MSA'] = compute_relative_error(est_matrix=M_est,
                                                          ref_matrix=M)

    relative_error['PALM4SMSA Id'] = []
    relative_error['G-PALM4SMSA Id'] = []
    relative_error['PALM4SMSA Rd'] = []
    relative_error['G-PALM4SMSA Rd'] = []
    relative_error['G-PALM4SMSA H'] = []
    res_factors = []
    for n_terms in n_terms_list:
        n_factors_per_term = n_factors // n_terms
        C = [[splincol(M.shape, lincol_sparsity)
              for _ in range(n_factors_per_term)]] * n_terms

        # run PALM4SMSA and G-PALM4SMSA using PALM4MSA with Id/0 initialisation
        I_Factors = [[np.identity(data_dim)] * (n_factors_per_term - 1)
                     + [np.zeros((data_dim, data_dim))]] * n_terms
        I_lambda = [1.0] * n_terms
        F2 = Palm4smsa(M, n_terms, n_factors_per_term, cons=C,
                       initfacts=asfortranarray2(I_Factors),
                       initlambda=I_lambda, N=n_iter_max)[0:2]
        relative_error['PALM4SMSA Id'].append(compute_relative_error(
            est_matrix=somme_multiplication(F2[0], F2[1]), ref_matrix=M
        ))
        F3 = Palm4smsa_hierarchique(M, n_terms, n_factors_per_term, cons=C,
                                    initfacts=asfortranarray2(I_Factors),
                                    initlambda=I_lambda,
                                    N=n_iter_max, N_1=n_iter_max)
        relative_error['G-PALM4SMSA Id'].append(compute_relative_error(
            est_matrix=somme_multiplication(F3[0], F3[1]), ref_matrix=M
        ))

        # run PALM4SMSA and G-PALM4SMSA using PALM4MSA with random initialisation
        relative_error['PALM4SMSA Rd'].append([])
        relative_error['G-PALM4SMSA Rd'].append([])
        for i in range(n_runs_init):
            I_Factors = [[np.random.randn(data_dim, data_dim)]
                         * n_factors_per_term] * n_terms
            F2_random = Palm4smsa(M, n_terms, n_factors_per_term,
                                  cons=C, initfacts=asfortranarray2(I_Factors),
                                  initlambda=I_lambda, N=n_iter_max)[0:2]
            relative_error['PALM4SMSA Rd'][-1].append(compute_relative_error(
                est_matrix=somme_multiplication(F2_random[0], F2_random[1]),
                ref_matrix=M
            ))
            F3_random = Palm4smsa_hierarchique(
                M, n_terms, n_factors_per_term, cons=C,
                initfacts=asfortranarray2(I_Factors), initlambda=I_lambda,
                N=n_iter_max, N_1=n_iter_max)
            relative_error['G-PALM4SMSA Rd'][-1].append(compute_relative_error(
                est_matrix=somme_multiplication(F3_random[0], F3_random[1]),
                ref_matrix=M
            ))

        # run G-PALM4SMSA using H-PALM4MSA (with Id initialisation)
        if n_factors_per_term == 1:
            F = [[splincol(M.shape, lincol_sparsity)]] * n_terms
            R = [[splincol(M.shape, 1)]] * n_terms
        else:
            F = [[splincol(M.shape, lincol_sparsity)
                  for _ in range(n_factors_per_term - 1)]] * n_terms
            R = [[splincol(M.shape, int(data_dim / (2 * i)))
                  for i in range(1, n_factors_per_term)]] * n_terms
        F3_h = Palm4smsa_hierarchique2(M, n_terms, n_factors_per_term,
                                       fact_cons=F, res_cons=R,
                                       N_1=n_iter_max, N_2=n_iter_max,
                                       N_3=n_iter_max)
        res_factors.append(F3_h)
        relative_error['G-PALM4SMSA H'].append(compute_relative_error(
            est_matrix=somme_multiplication(F3_h[0], F3_h[1]),
            ref_matrix=M
        ))

    return relative_error, n_terms_list, res_factors
    ########


def get_list_of_n_factors(data_dim, extended_list=False):
    n_min = int(np.ceil(np.log2(data_dim)))
    n_range = list(range(n_min, int(data_dim / 2)))
    n_divisors = [len(divisors(n)) for n in n_range]

    n_div_max = -1
    n_range_max = []
    n_divisors_max = []
    for i in range(len(n_range)):
        if n_divisors[i] >= n_div_max:
            if n_divisors[i] > n_div_max or extended_list:
                n_div_max = n_divisors[i]
                n_range_max.append(n_range[i])
                n_divisors_max.append(n_divisors[i])
                print(f'{n_range[i]} has {n_divisors[i]} divisors.')
    return n_range_max


if __name__ == '__main__':
    import pickle
    res = {}
    for data_size in (32, 64, 128):
        print('*' * 80)
        print(f'Data size {data_size}')
        print('*' * 80)
        for n_factors in get_list_of_n_factors(data_dim=data_size):
            print('=' * 60)
            print(f'{n_factors} factors')
            print('=' * 60)
            if n_factors * data_size * 2 > data_size ** 2:
                break
            relative_error, n_terms_list, res_factors = \
                experience(data_dim=data_size, n_factors=n_factors,
                           n_runs_init=0)
            res[data_size, n_factors] = relative_error, n_terms_list
            with open('res_icassp_nb_factors.pickle', 'wb') as file:
                pickle.dump(res, file)

            # filename = f'icassp_{data_size}_{n_factors}'
            # title = f'Data size : {data_size} - Budget : {n_factors}'
            # for additional_curves in (False, True):
            #     plot_results(relative_error, n_terms_list,
            #                  filename=filename, title=title,
            #                  additional_curves=additional_curves)
