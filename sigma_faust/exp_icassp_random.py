# -*- coding: utf-8 -*-
# ######### COPYRIGHT #########
# Credits
# #######
#
# Copyright(c) 2019-2021
# ----------------------
#
# * Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
# * Institut de Mathématiques de Marseille <https://www.i2m.univ-amu.fr/>
# * Université d'Aix-Marseille <http://www.univ-amu.fr/>
# * Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
# * Université de Toulon <http://www.univ-tln.fr/>
#
# Contributors
# ------------
#
# * Moujahid Bou-Laouz
# * Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
#
# Description
# -----------
#
# `sigma_faust` is a Python implementation of algorithms and experiments proposed
#  in paper *Learning a sum of sparse matrix products* by Moujahid Bou-Laouz,
#  Valentin Emiya, Liva Ralaivola and Caroline Chaux in 2021.
#
#
# Version
# -------
#
# * sigma_faust version = 0.1
#
# Licence
# -------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ######### COPYRIGHT #########
"""

.. moduleauthor:: Valentin Emiya
"""
import pickle

from sigma_faust.exp_icassp_nb_factors import experience

res = {}
data_size = 256
print('*' * 80)
print(f'Data size {data_size}')
print('*' * 80)
for n_factors in [8, 24]:
    print('=' * 60)
    print(f'{n_factors} factors')
    print('=' * 60)
    if n_factors * data_size * 2 > data_size ** 2:
        break
    relative_error, n_terms_list, res_factors = \
        experience(data_dim=data_size, n_factors=n_factors,
                   n_runs_init=0)
    res[data_size, n_factors] = relative_error, n_terms_list
    with open('res_icassp_nb_factors_256.pickle', 'wb') as file:
        pickle.dump(res, file)
